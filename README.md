# Plasma Mobile phonebook

Contacts application which allows adding, modifying and removing contacts.

# Backend

The application uses kpeople models to find, add, remove and update contacts.
The actual vcard editing is done using KContacts, allowing to use the full range of fields vcards support.
Plasma Phonebook ships its own KPeople plugin for displaying actions.

